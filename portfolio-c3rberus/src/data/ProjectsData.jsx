export default [
    {
        id: 1,
        name: "Veterinary Manager",
        desc: `Veterinary Patient Manager is an application that facilitates the management of veterinary patients, using MERN STACK through an application in which they can keep track of their patients the registration via email is working through nodemailer. WAIT 1 MINUTE IN LOGIN.`,
        image: '/img/APV.png',
        demoLink: "https://apv.c3rberus.dev",
        gitLink: "https://gitlab.com/CerberusGamer27/apv_mern_frontend",
        extraInfo: "Email: admin@gmail.com Password: password",
        stack: ["React", "Tailwind", "Node JS", "MongoDB"]
    },
    {
        id: 2,
        name: "VM React Native",
        desc: "The same app project but rewritten in React Native to be able to use it in mobile version, making some changes: Changing the backend from Node to Firebase to have a better accessibility, and adding vaccines, appointments, deworming, etc. To have a better control of the pets" +
                " (Live Demo does not work due to a problem with expo snack with Firebase)",
        image: '/img/APVMobile.png',
        demoLink: "https://snack.expo.dev/@c3rberus/e73e65",
        gitLink: "https://github.com/Cerberus270/dama-project",
        stack: ["ReactNative", "Firebase", "NativeBase", "ReactRouterNative"]
    },
    {
        id: 3,
        name: "Portfolio",
        desc: "My Portfolio Website, to show my works, and practice my skills, implementing dark mode switch, and custom 404 page.",
        image: '/img/PortfolioC3rberus.png',
        demoLink: "https://c3rberus.dev",
        gitLink: "https://gitlab.com/CerberusGamer27/portfolio-react/-/tree/main/portfolio-c3rberus",
        stack: ["React", "Tailwind", "ReactRouter"]
    },
    {
        id: 4,
        name: "Store 24",
        desc: "Application made in React TS and CHAKRA UI for the management of products, can be installed since the VitePWA plugin was added to meet the standard of a PWA, use React Hook Forms to form validation and has a little product search engine, I use props to control information between components.",
        image: '/img/store24.png',
        demoLink: "https://ubiquitous-smakager-85b2fa.netlify.app/",
        gitLink: "https://gitlab.com/CerberusGamer27/store-frontend-ts",
        stack: ["React", "ChakraUI", "ReactHookForms", "Toastify"]
    },
    {
        id: 5,
        name: "Travel Agency",
        desc: "Landing page for a travel agency, showing the tourist destinations of the agency, information and testimonials that are loaded by a form and stored in a DB implementing an ORM, includes routing through express, the whole site uses MVC architecture.",
        image: '/img/travelAgency.png',
        demoLink: "https://viajes.c3rberus.dev",
        gitLink: "https://github.com/Cerberus270/travel-agency",
        stack: ["NodeJS", "ExpressJS", "Sequelize", "PUG"]
    }
]