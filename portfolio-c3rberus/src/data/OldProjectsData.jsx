export default [
    {
        id: 1,
        name: "Climatic Change",
        desc: "Landing page for an NGO that raises awareness about the effects of climate change through varied multimedia content, written entirely in HTML, JS ES6 and CSS, also consumes 3 APIs that help us determine the A.Q.I index, carbon emissions for our electricity consumption, etc.. It includes a gallery and informative videos with Lazy Loading.",
        image: '/img/climaticChange.png',
        demoLink: "https://test.c3rberus.dev",
        gitLink: "https://github.com/Cerberus270/ProyectoDWA",
        stack: ["HTML", "CSS", "Bootstrap", "JQuery", "FetchAPI"]
    },
    {
        id: 2,
        name: "LinkTree Bio",
        desc: "My own linktree implementation made in HTML, CSS and JS, includes animations with JS, with a loading animation, being fully responsive.",
        image: '/img/links.png',
        demoLink: "https://links.c3rberus.dev/",
        gitLink: "https://gitlab.com/CerberusGamer27/link-in-bio",
        stack: ["HTML", "CSS", "Bootstrap", "Three.JS"]
    },
    {
        id: 3,
        name: "Simple PWA App",
        desc: "A simple web PWA, the work with this application was to configure the ServiceWorker, to allow it to be installable on all devices.",
        image: '/img/pwa.png',
        demoLink: "https://pwa.c3rberus.dev/",
        gitLink: "https://gitlab.com/",
        stack: ["HTML", "CSS", "JS"]
    }
]