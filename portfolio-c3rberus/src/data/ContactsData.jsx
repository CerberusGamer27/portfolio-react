export default [
    {
        id: 1,
        name: "LinkedIn",
        iconName: "linkedin",
        contactLink: "https://www.linkedin.com/in/cerberusdev/"
    },
    {
        id: 2,
        name: "Github",
        iconName: "github",
        contactLink: "https://github.com/Cerberus270"
    },
]