import { useState } from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { MainLayout } from './layout/MainLayout'
import About from './pages/About'
import { Home } from './pages/Home'
import NotFound from './pages/NotFound'
import { Works } from './pages/Works'

function App() {

  return (
    <BrowserRouter>
      <Routes>
        <Route path='*' element={<NotFound />} />
        <Route path='/' element={<MainLayout />}>
          <Route index
            element={<Home />} />
          <Route path='works'
            element={<Works />} />
          <Route path='about'
            element={<About />} />
        </Route>
      </Routes>
    </BrowserRouter>
  )
}

export default App
