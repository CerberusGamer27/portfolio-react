import React from 'react'
import HeaderH2 from '../components/HeaderH2'

const About = () => {
    return (
        <div className="max-w-3xl w-11/12 mx-auto min-h-screen">
            <HeaderH2 children={"How This Site Work?"} />
            <p className='dark:text-zinc-100 text-justify'>
                This site is hosted in Oracle Cloud, using an {' '}
                <a className='text-indigo-600 hover:underline cursor-pointer' href='https://www.oracle.com/cloud/compute/arm/'
                    target={'_blank'}>Oracle ARM Ampere A1 CPU</a>,
                this demonstrating the high performance of this architecture, using apache2
                as domain server with several subdomains, and configured to run React Router DOM,
                having to configure the ports to be able to access the information and prevent possible security breaches,
                then we have Cloudflare responsible for providing protection to our site and ensure
                that it is accessible with my domain.
            </p>
            <br />
            <p className='dark:text-zinc-100 text-justify'>
                Demos of the projects shown in my portfolio with the exception of React Native and Store run on this server,
                this includes the DBs with the exception of Mongo DB, which is on a Mongo DB Atlas instance.
            </p>
            <HeaderH2 children={"Why Not Vercel o Netlify?"} />
            <p className='dark:text-zinc-100 text-justify mt-5'>
                The objective of having the projects hosted in a more 'classic' way is to know what problems can give an application in production environments, using any type of architecture, for example the configuration of apache2 to make react-router work, is similar to the one that is done in vercel, with the file
                vercel.config.js, although I have also tested in vercel, to know all the possible configurations and test the applications in different environments.
            </p>
            <HeaderH2 children={"Diagram"} />
            <p className='dark:text-zinc-100 text-justify my-5 font-mono '>
            This is the architecture diagram of my site in a summarized way.
            </p>
            <div>
                <img className='shadow-lg' src={'/img/stackDark.png'} />
            </div>
        </div>
    )
}

export default About