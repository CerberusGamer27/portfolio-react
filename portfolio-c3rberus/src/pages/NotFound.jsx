import React from 'react'
import { Link } from 'react-router-dom'

const NotFound = () => {
    return (
        <div className="bg-hero_pattern min-h-screen bg-no-repeat bg-cover bg-center bg-fixed">
            <div className="grid h-screen place-items-center">
                <div className='text-center bg-slate-800 bg-opacity-25 p-5 rounded-md'>
                    <h1 className='font-bold text-9xl text-white'>
                        404
                    </h1>
                    <p className='text-white font-mono font-semibold'>
                        There is nothing here
                    </p>
                    <p className='text-white font-semibold'>
                        The page you are looking for does not exist or maybe you do not exist.
                    </p>
                    <button className='bg-blue-500 hover:bg-blue-700 text-white 
                    p-2 rounded-xl my-5 shadow-md hover:underline'>
                        <Link to={'/'}>
                            <p>Back to Home Page</p>
                        </Link>
                    </button>
                </div>
            </div>

        </div>
    )
}

export default NotFound