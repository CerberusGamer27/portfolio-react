import React from 'react'
import HeaderH2 from '../components/HeaderH2'
import { Projects } from '../components/Projects'
import { OldProjects } from '../components/OldProjects'

export const Works = () => {
    return (
        <div className="max-w-3xl w-11/12 mx-auto min-h-screen">
            <HeaderH2 children={'Works'} />
            <p className='text-center font-mono text-lg pb-5 dark:text-white'>
                Click on the image to see a Live Preview
            </p>
            <Projects />
            <HeaderH2 children={'Old Works'} />
            <OldProjects />
        </div>
    )
}