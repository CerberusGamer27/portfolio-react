import HeroSection from '../components/HeroSection'
import Bio from '../components/Bio';
import HeaderH2 from '../components/HeaderH2';
import FreeTime from '../components/FreeTime';
import Contact from '../components/Contact';

export const Home = () => {

    return (
        <>
            
            <div className="max-w-3xl w-11/12 mx-auto">
                <HeroSection />
                <HeaderH2 children={'Bio'} />
                <Bio />
                <HeaderH2 children={'I ❤'} />
                <FreeTime />
                <HeaderH2 children={'On the web'}/>
                <Contact />
            </div>
        </>
    )
}
