import { ChevronDownIcon, ChevronRightIcon } from '@heroicons/react/24/outline'
import { Link } from 'react-router-dom';

const ResumeButton = (props) => {
    return (
        <HomeButtonWrap>
            <a href='https://docs.google.com/document/d/1ukX_MbJerzvfcu4f40rytPz2tbT2WvOwgIg8MBzFkXY/edit?usp=share_link' target={'_blank'}>
                <div className="flex justify-end pl-2 gap-3
                            font-normal text-base uppercase dark:text-white tracking-loose">
                    <p className='hover:underline cursor-pointer'>
                    {props.value}
                    </p>
                    <ChevronRightIcon className="h-6 w-6 text-blue-500" />
                </div>
            </a>
        </HomeButtonWrap>
    )
}

const HomeButtonWrap = ({ children }) => {
    return (
        <div
            className="flex items-center justify-center flex-col px-4 py-3 my-10 rounded-full
                bg-zinc-100 dark:bg-zinc-700 shadow-sm
                hover:bg-slate-300 dark:hover:bg-zinc-600"
        >
            {children}
        </div>
    )
}

export default ResumeButton