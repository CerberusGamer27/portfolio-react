import React from 'react'
import ProjectsData from '../data/ProjectsData'
import GitHub from './icons/GitHub'

const Item = ({ data }, key) => {

    return (
        <div key={key} className="max-w-sm overflow-hidden rounded-lg shadow-lg bg-white dark:bg-zinc-600">
            <a href={data.demoLink} target={'_blank'}>
                <img className="w-full" src={data.image} alt={data.name} />
            </a>
            <div className="px-6 py-4">
                <div className="font-bold text-xl mb-2 dark:text-gray-100">
                    {data.name} {''}
                    <a href={data.gitLink} target={'_blank'} className={"after:content-['_↗']"}>
                        <img className='inline' src={data.gitLink.search("gitlab")> -1 ? '/img/gitlab-logo-500.png': '/img/github-2.png'} height={"50"} width={"50"} />
                    </a>
                </div>
                <p className="text-gray-700 dark:text-gray-200 text-base">
                    {data.desc}
                </p>
                {
                    data?.extraInfo? <p className="text-emerald-600 dark:text-emerald-300 text-base py-5">{data.extraInfo}</p> : null
                }
            </div>
            <div className="px-6 pt-4 pb-2">
                {data.stack.map((stack, i) => (
                    <span key={i} className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#{stack}</span>
                ))}
            </div>
        </div>
    )
}

export const Projects = () => {
    return (
        <div className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-2 gap-4 auto-cols-auto">
            {ProjectsData.map((data, i) => (
                < Item key={i} data={data} />
            ))}

        </div>
    )
}
