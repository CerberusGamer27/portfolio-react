import React from 'react'
import ResumeButton from './ResumeButton'

const Bio = () => {
    return (
        <>
            <div className="dark:bg-zinc-700 bg-zinc-100 shadow sm:rounded-lg mb-5">
            {/* <div className="px-4 py-5 sm:px-6">
                <h3 className="text-lg font-medium leading-6 text-white">Applicant Information</h3>
                <p className="mt-1 max-w-2xl text-sm text-gray-200">Personal details and application.</p>
            </div> */}
            {/* <div className="border-t border-gray-600"> */}
                <dl>
                    <div className="dark:bg-zinc-700 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                        <dt className="text-sm font-bold dark:text-white">2000</dt>
                        <dd className="mt-1 font-mono text-sm dark:text-white sm:col-span-2 sm:mt-0">Born in El Salvador.</dd>
                    </div>
                    <div className="dark:bg-zinc-700 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                        <dt className="text-sm font-bold dark:text-white">2016 - 2018</dt>
                        <dd className="mt-1 font-mono text-sm dark:text-white sm:col-span-2 sm:mt-0">Graduated from high school in electronics with a diploma in programming.</dd>
                    </div>
                    <div className="dark:bg-zinc-700 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                        <dt className="text-sm font-bold dark:text-white">2019 - Now</dt>
                        <dd className="mt-1 font-mono text-sm dark:text-white sm:col-span-2 sm:mt-0">Studying Software Development Engineering.</dd>
                    </div>
                </dl>
            {/* </div> */}
        </div>
        <ResumeButton value={"Check My Resume"}/>
        </>

    )
}

export default Bio