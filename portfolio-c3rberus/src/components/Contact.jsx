import React from 'react'
import Linkedin from './icons/Linkedin'
import GitHub from './icons/GitHub'
import Gitlab from './icons/Gitlab'


const Contact = () => {

    return (
        <div id="links" role="navigation">
            <ul>
                {/* Dynamic List is Working but i need the icons of social networks  */}

                {/* {Contacts.map(con => (
                    <li key={con.id}>
                    <a className="link-container" href={`${con.contactLink}`} target="_blank">
                    <span id="label" className='text-zinc-200 font-bold'>{con.name}</span>
                    </a>
                    </li>
                ))} */}
                <li>
                    <a className="link-container" href="https://www.linkedin.com/in/cerberusdev/" target="_blank">
                        <Linkedin />
                        <span id="label" className='dark:text-zinc-200 text-zinc-800 font-bold underline'>Linkedin</span>
                    </a>
                </li>
                <li>
                    <a className="link-container" href="https://github.com/Cerberus270" target="_blank">
                        <GitHub />
                        <span id="label" className='dark:text-zinc-200 text-zinc-800 font-bold underline'>GitHub</span>
                    </a>
                </li>
                <li>
                    <a className="link-container" href="https://gitlab.com/CerberusGamer27" target="_blank">
                        <Gitlab />
                        <span id="label" className='dark:text-zinc-200 text-zinc-800 font-bold underline'>Gitlab</span>
                    </a>
                </li>
            </ul>
        </div>
    )
}

export default Contact