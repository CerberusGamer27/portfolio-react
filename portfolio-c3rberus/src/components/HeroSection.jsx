import React from 'react';
import profilePicture from '../assets/profile.jpeg'
import HomeButton from './HomeButton';

const HomeImgWrap = ({ children }) => {
	return (
		<div
			className="sm:w-40 sm:h-40 w-32 h-32 my-5 rounded-full flex place-content-center
					dark:bg-zinc-600 bg-zinc-50 shadow-sm">
			{children}
		</div>
	)
}

const HomeImg = () => {
	return (
		<img
			src={profilePicture}
			className="sm:w-32 sm:h-32 h-28 w-28 rounded-full self-center"
			alt='Profile Picture of Cerberus'
		/>
	)
}

function HeroSection() {
	return (
		<div id="hero" className="flex items-center justify-center flex-col py-5">
			<div className="flex h-16 items-center justify-center">
				<div className="px-4 py-3 my-10 rounded-md
                bg-zinc-100 dark:bg-zinc-700 shadow-xl
                hover:bg-zinc-200 dark:hover:bg-zinc-600 ">
					<p className='text-center font-mono dark:text-white'>Hello, and welcome to my portfolio, nice to see you here</p>
				</div>
			</div>
			<HomeImgWrap>
				<HomeImg />
			</HomeImgWrap>
			<div className="text-center">
				<h1 className="text-2xl md:text-4xl mb-1 md:mb-3 text-indigo-600 dark:text-indigo-500 font-semibold">
					Geovanny Avila
				</h1>
				<p className="text-md md:text-xl mb-3 dark:text-white text-justify">
					I'm a Software Development Engineering Student from El Salvador 🇸🇻 and I like to develop applications that make life easier for us.
					I'm always learning about the most modern technologies, like React JS, Node JS, Python and currently learning AstroBuild.{' '}
				</p>
			</div>
			<HomeButton value="See my work" />
		</div>
	);
}

export default HeroSection;