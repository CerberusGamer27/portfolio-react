import { Disclosure } from '@headlessui/react'
import { Bars3Icon, XMarkIcon } from '@heroicons/react/24/outline'
import { Link, NavLink } from 'react-router-dom'


const navigation = [
    { name: 'Works', href: 'works', current: false },
    { name: 'About', href: 'about', current: false },
    { name: 'Custom 404', href: '404', current: false },
]

function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
}

const NavBar = () => {
    return (
        <Disclosure as="nav" className="bg-stone-100 dark:bg-neutral-800 sticky top-0">
            {({ open }) => (
                <>
                    <div className="mx-auto max-w-7xl px-2 sm:px-6 lg:px-8">
                        <div className="relative flex h-16 items-center justify-center">
                            <div className="absolute inset-y-0 left-0 flex items-center sm:hidden">
                                {/* Mobile menu button*/}
                                <Disclosure.Button className="inline-flex items-center justify-center rounded-md p-2 text-gray-400 hover:bg-gray-700 hover:text-white focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white">
                                    <span className="sr-only">Open main menu</span>
                                    {open ? (
                                        <XMarkIcon className="block h-6 w-6" aria-hidden="true" />
                                    ) : (
                                        <Bars3Icon className="block h-6 w-6" aria-hidden="true" />
                                    )}
                                </Disclosure.Button>
                            </div>
                            <div className="flex flex-1 items-center justify-center sm:items-stretch sm:justify-center">
                                <div className="flex flex-shrink-0 items-center">
                                    <Link to={'/'}>
                                        <img
                                            className="block h-8 w-auto lg:hidden hover:animate-pulse"
                                            src="/img/gundam.png"
                                            alt="c3rberus.dev"
                                            width={32} height={32}
                                        />
                                        <img
                                            className="hidden h-8 w-auto lg:block hover:animate-pulse"
                                            src="/img/gundam.png"
                                            alt="c3rberus.dev"
                                            width={32} height={32}
                                        />
                                    </Link>
                                </div>
                                <div className="hidden sm:ml-6 sm:block">
                                    <div className="flex space-x-4">
                                        {navigation.map((item) => (
                                            <NavLink key={item.name} to={item.href} aria-current={item.current ? 'page' : undefined}
                                                className={({ isActive }) => isActive ? "bg-stone-400 rounded-md px-3 py-2 dark:bg-gray-900 text-sm font-medium" :
                                                    'text-gray-300 hover:bg-zinc-400 hover:text-white px-3 py-2 rounded-md text-sm font-medium'}>
                                                <p className='dark:text-white text-black'>
                                                    {item.name}
                                                </p>
                                            </NavLink>
                                        ))}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <Disclosure.Panel className="sm:hidden">
                        <div className="space-y-1 px-2 pt-2 pb-3">
                            {navigation.map((item) => (
                                <NavLink key={item.name} to={item.href} aria-current={item.current ? 'page' : undefined}
                                    className={({ isActive }) => isActive ? "block bg-stone-400 rounded-md px-3 py-2 dark:bg-gray-900 text-sm font-medium" :
                                        'block text-gray-300 hover:bg-zinc-400 hover:text-white px-3 py-2 rounded-md text-sm font-medium'}>
                                    <p className='dark:text-white text-black'>
                                        {item.name}
                                    </p>
                                </NavLink>
                            ))}
                        </div>
                    </Disclosure.Panel>
                </>
            )}
        </Disclosure>
    )
}

export default NavBar