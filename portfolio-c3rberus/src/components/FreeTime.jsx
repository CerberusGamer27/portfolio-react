import React from 'react'

const FreeTime = () => {
  return (
    <div>
        <p className='dark:text-white font-mono'>
          <a href={'https://steamcommunity.com/id/cerberusgamer27/'} target={'_blank'} 
          className='dark:text-indigo-400 text-indigo-600 hover:underline underline-offset-4'>Play video games</a>
        , read about PC hardware, watch documentaries about history and science.</p>
    </div>
  )
}

export default FreeTime