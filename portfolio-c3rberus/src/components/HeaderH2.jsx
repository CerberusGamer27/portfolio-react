import React from 'react'

const HeaderH2 = ({children}) => {
    return (
        <h2 className="flex flex-row flex-nowrap items-center my-8">
            <span className="flex-grow block border-t border-zinc-100 dark:border-zinc-700" aria-hidden="true" role="presentation"></span>
            <span className="flex-none block mx-4 px-4 py-2.5 text-xs leading-none 
                            font-medium uppercase bg-zinc-100 dark:bg-zinc-700 dark:text-white
                            rounded-md">
                {children}
            </span>
            <span className="flex-grow block border-t border-zinc-100 dark:border-zinc-700" aria-hidden="true" role="presentation"></span>
        </h2>
    )
}

export default HeaderH2