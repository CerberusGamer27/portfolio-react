import { ChevronDownIcon, ChevronRightIcon } from '@heroicons/react/24/outline'
import { Link } from 'react-router-dom';

const HomeButton = (props) => {
    return (
        <HomeButtonWrap>
            <Link to={'works'}>
                <div className="flex justify-end pl-2 gap-3
                            font-normal text-base uppercase dark:text-white tracking-loose">
                    <p className='hover:underline cursor-pointer'>
                        {props.value}
                    </p>
                    <ChevronRightIcon className="h-6 w-6 text-blue-500" />
                </div>
            </Link>
        </HomeButtonWrap>
    )
}

const HomeButtonWrap = ({ children }) => {
    return (
        <div
            className="block px-4 py-3 my-10 rounded-full
                bg-zinc-100 dark:bg-zinc-700 shadow-sm
                hover:bg-slate-300 dark:hover:bg-zinc-600"
        >
            {children}
        </div>
    )
}

export default HomeButton