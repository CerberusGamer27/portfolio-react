import { React, useState, useEffect } from 'react'
import { Outlet } from 'react-router-dom'
import NavBar from '../components/Nav.jsx'


export const MainLayout = () => {
    const [theme, setTheme] = useState(null);

    useEffect(() => {
        if (localStorage.theme === 'dark' || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
            setTheme('dark')
        } else {
            setTheme('light')
        }
    }, []);

    const handleThemeSwitch = () => {
        setTheme(theme === 'dark' ? 'light' : 'dark');
        localStorage.setItem('theme', theme === 'dark' ? 'light' : 'dark');
    };

    useEffect(() => {
        if (theme === 'dark') {
            document.documentElement.classList.add('dark');
        } else {
            document.documentElement.classList.remove('dark');
        }
    }, [theme])

    return (
        <>
            <div className="bg-neutral-200 dark:bg-neutral-800 flex flex-col pb-10">

                <NavBar />
                <button
                    type="button"
                    onClick={handleThemeSwitch}
                    className="fixed z-10 right-2 top-2 bg-violet-500 hover:bg-violet-600 text-lg p-1 rounded-md">
                    {theme === 'dark' ? '🌙' : '🌞'}
                </button>

                <Outlet />

            </div>
        </>
    )
}
