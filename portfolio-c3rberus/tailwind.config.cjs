/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: 'class',
  content: ["index.html", "./src/**/*.jsx"],
  theme: {
    extend: {
      backgroundImage:
      {
        'hero_pattern': "url('/img/eldenRing.jpg')"
      }
    },
  },
  plugins: [],
}
